import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';

import 'package:rosseti_application/accept_phone_page.dart';

import 'package:rosseti_application/futures/registration_future.dart';

import 'package:rosseti_application/models/phone_model.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPage createState() => _RegistrationPage();
}

class _RegistrationPage extends State<RegistrationPage> {
  final TextEditingController telController = TextEditingController();

  bool isValidate = false;

  ///для валидации
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              ///для валидации
              key: _formKey,
              child: ListView(
                //*scrollDirection: Axis.vertical,*//*
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(30, 0, 30, 16),
                children: [
                  TextFormField(
                      controller: telController,
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        hintText: 'Телефон',
                        labelStyle: TextStyle(fontSize: 20),
                        prefixIcon: Icon(
                          Icons.phone,
                          color: new Color.fromRGBO(32, 86, 146, 0.8),
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide: BorderSide(
                              color: new Color.fromRGBO(32, 86, 146, 1),
                            )),
                        filled: true,
                      ),
                      keyboardType: TextInputType.phone,

                      ///для валидации
                      validator: _validTel),
                ],
              ),
            ),
            SizedBox(
              width: 300,
              child: FloatingActionButton.extended(
                elevation: 3,
                onPressed: () async {
                  _submitForm();
                  if (isValidate) {
                    final PhoneModel user = await authphone(telController.text);
                    Route route =
                        MaterialPageRoute(builder: (context) => AcceptPage());
                    Navigator.push(context, route);
                  }
                },
                label: Text(
                  'Далее',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 0.8),
                  ),
                ),
                backgroundColor: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _submitForm() {
    if (_formKey.currentState.validate())
      isValidate = true;
    else
      isValidate = false;
  }

  ///для валидации
  String _validTel(String value) {
    final _phonesample = RegExp(r'^\d\d\d\-\d\d\d\-\d\d\-\d\d$');
    if (value.isEmpty)
      return 'Введите телефон';
    else if (!_phonesample.hasMatch(value))
      return 'Поле должно быть типа 777-777-77-77';
    else
      return null;
  }
}
