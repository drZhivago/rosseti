import 'dart:convert';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../futures/user_future.dart';

import '../models/user_model.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPage createState() => _UserPage();
}

class _UserPage extends State<UserPage> {
  @override
  void initState() {
    loadData();
    super.initState();
  }

  var _postJson;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Мой статус',
              style: TextStyle(
                color: new Color.fromRGBO(32, 86, 146, 1),
                fontSize: 30,
              ),
            ),
            Image.asset(
              'assets/images/crowns 1.png',
              width: 124,
              height: 124,
            ),
            Text(
              'Серебрянный статус',
              style: TextStyle(
                color: new Color.fromRGBO(32, 86, 146, 0.9),
                fontSize: 20,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Имя',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null && _postJson["full_name"] != null
                          ? '${_postJson["full_name"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Телефон',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null && _postJson["phone"] != null
                          ? '${_postJson["phone"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Всего комментариев',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null && _postJson["comments_count"] != null
                          ? '${_postJson["comments_count"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Ваш рейтинг',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null && _postJson["ratings_count"] != null
                          ? '${_postJson["ratings_count"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Принятых предложений',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null &&
                              _postJson["accepted_proposals_count"] != null
                          ? '${_postJson["accepted_proposals_count"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Отклонённых предложений',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null &&
                              _postJson["denied_proposals_count"] != null
                          ? '${_postJson["denied_proposals_count"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Всего предложений',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      _postJson != null && _postJson["proposals_count"] != null
                          ? '${_postJson["proposals_count"]}'
                          : '',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        fontSize: 20,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 35,
            ),
            SizedBox(
              width: 300,
              child: FloatingActionButton.extended(
                elevation: 3,
                onPressed: () {
                  Navigator.pop(context);
                },
                label: Text(
                  'Готово',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 0.8),
                  ),
                ),
                backgroundColor: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void loadData() {
    getUser().then((response) {
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);

        setState(() {
          _postJson = jsonData;
        });
      } else {
        print(response.statusCode);
      }
    });
  }
}
