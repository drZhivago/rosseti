import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rosseti_application/futures/comment_future.dart';
import 'package:rosseti_application/futures/user_future.dart';
import 'package:rosseti_application/models/comment_model.dart';
import 'package:rosseti_application/models/project_model.dart';

class CommentsPage extends StatefulWidget {
  Suggestions suggestions;

  CommentsPage({this.suggestions});

  @override
  _CommentsPage createState() => _CommentsPage(suggestions: suggestions);
}

class _CommentsPage extends State<CommentsPage> {
  Suggestions suggestions;

  _CommentsPage({this.suggestions});

  final TextEditingController textController = TextEditingController();

  @override
  void initState() {
    loadData();
    super.initState();
  }

  var _userid;

  CommentModel comment;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 35,
            horizontal: 8,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios_outlined),
                    color: new Color.fromRGBO(32, 86, 146, 1.0),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Text(
                    "Обсуждение",
                    style: TextStyle(
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      fontSize: 29,
                    ),
                  ),
                  IconButton(
                    icon: Image.asset(
                      'assets/images/image 1.png',
                      width: 150,
                      height: 150,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/userpage');
                    },
                  ),
                ],
              ),
              comment == null
                  ? Expanded(
                      child: _userid != null && _userid["id"] != null
                          ? ListView.builder(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: suggestions.comments.length,
                              itemBuilder: (context, index) {
                                if (_userid["id"] !=
                                    suggestions.comments[index].user.id) {
                                  return Card(
                                    color: Colors.white,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(22.0),
                                        bottomLeft: Radius.circular(22.0),
                                        topRight: Radius.circular(22.0),
                                      ),
                                      side: BorderSide(
                                          color: Colors.white70, width: 1),
                                    ),
                                    child: ListTile(
                                      minVerticalPadding: 15,
                                      minLeadingWidth: 15,
                                      //isThreeLine: true,
                                      contentPadding: EdgeInsets.all(5),
                                      title: Text(
                                        '${suggestions.comments[index].user.fullName}',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12),
                                      ),
                                      subtitle: Text(
                                        '${suggestions.comments[index].text}',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 17),
                                      ),
                                    ),
                                  );
                                } else {
                                  return Card(
                                    color: new Color.fromRGBO(32, 86, 146, 1),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(22.0),
                                        bottomLeft: Radius.circular(22.0),
                                        topLeft: Radius.circular(22.0),
                                      ),
                                      side: BorderSide(
                                          color: Colors.white70, width: 1),
                                    ),
                                    child: ListTile(
                                      minVerticalPadding: 15,
                                      minLeadingWidth: 15,
                                      //isThreeLine: true,
                                      contentPadding: EdgeInsets.all(5),
                                      trailing: Text(
                                        'Вы',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                      subtitle: Text(
                                        '${suggestions.comments[index].text}',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  );
                                }
                              })
                          : Center(child: CircularProgressIndicator()))
                  : Expanded(
                      child: _userid != null && _userid["id"] != null
                          ? ListView.builder(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: comment.comments.length,
                              itemBuilder: (context, index) {
                                if (_userid["id"] !=
                                    comment.comments[index].user.id) {
                                  return Card(
                                    color: Colors.white,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(22.0),
                                        bottomLeft: Radius.circular(22.0),
                                        topRight: Radius.circular(22.0),
                                      ),
                                      side: BorderSide(
                                          color: Colors.white70, width: 1),
                                    ),
                                    child: ListTile(
                                      minVerticalPadding: 15,
                                      minLeadingWidth: 15,
                                      //isThreeLine: true,
                                      contentPadding: EdgeInsets.all(5),
                                      title: Text(
                                        '${comment.comments[index].user.fullName}',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12),
                                      ),
                                      subtitle: Text(
                                        '${comment.comments[index].text}',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 17),
                                      ),
                                    ),
                                  );
                                } else {
                                  return Card(
                                    color: new Color.fromRGBO(32, 86, 146, 1),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(22.0),
                                        bottomLeft: Radius.circular(22.0),
                                        topLeft: Radius.circular(22.0),
                                      ),
                                      side: BorderSide(
                                          color: Colors.white70, width: 1),
                                    ),
                                    child: ListTile(
                                      minVerticalPadding: 15,
                                      minLeadingWidth: 15,
                                      //isThreeLine: true,
                                      contentPadding: EdgeInsets.all(5),
                                      trailing: Text(
                                        'Вы',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                      subtitle: Text(
                                        '${comment.comments[index].text}',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  );
                                }
                              })
                          : Center(child: CircularProgressIndicator())),
              //Comments(suggestions: suggestions, comment: comment,),
              Divider(
                height: 2,
              ),
              Container(
                width: 300,
                child: TextFormField(
                  minLines: 1,
                  maxLines: 3,
                  controller: textController,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    hintText: 'Ваше сообщение',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.grey, width: 1)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.grey, width: 1)),
                    suffixIcon: Container(
                      margin: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
                      width: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        color: new Color.fromRGBO(32, 86, 146, 1),
                      ),
                      child: IconButton(
                        onPressed: () async {
                          if (textController.text != '') {
                            comment = await addcomment(
                              suggestions.id.toString(),
                              textController.text,
                            );
                            textController.clear();
                          } else
                            return null;
                          setState(() {});
                        },
                        color: Colors.white,
                        icon: Icon(Icons.arrow_upward),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void loadData() {
    getUser().then((response) {
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);
        setState(() {
          _userid = jsonData;
        });
      } else {
        print(response.statusCode);
      }
    });
  }
}

/*class Comments extends StatefulWidget {
  Suggestions suggestions;
  CommentModel comment;
  Comments({this.suggestions, this.comment});

  @override
  _Comments createState() => _Comments(suggestions: suggestions, comment: comment);
}

class _Comments extends State<Comments>{

  CommentModel comment;

  Suggestions suggestions;

  _Comments({this.suggestions, this.comment});

  var _userid;

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return comment == null ?
    Expanded(
        child: _userid != null && _userid["id"] != null
            ? ListView.builder(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: suggestions.comments.length,
            itemBuilder: (context, index) {
              if (_userid["id"] != suggestions.comments[index].user.id) {
                return Card(
                  color: Colors.white,
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                      bottomLeft: Radius.circular(22.0),
                      topRight: Radius.circular(22.0),
                    ),
                    side: BorderSide(color: Colors.white70, width: 1),
                  ),
                  child: ListTile(
                    minVerticalPadding: 15,
                    minLeadingWidth: 15,
                    //isThreeLine: true,
                    contentPadding: EdgeInsets.all(5),
                    title: Text(
                      '${suggestions.comments[index].user.fullName}',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                    subtitle: Text(
                      '${suggestions.comments[index].text}',
                      style: TextStyle(color: Colors.black, fontSize: 17),
                    ),
                  ),
                );
              } else {
                return Card(
                  color: new Color.fromRGBO(32, 86, 146, 1),
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                      bottomLeft: Radius.circular(22.0),
                      topLeft: Radius.circular(22.0),
                    ),
                    side: BorderSide(color: Colors.white70, width: 1),
                  ),
                  child: ListTile(
                    minVerticalPadding: 15,
                    minLeadingWidth: 15,
                    //isThreeLine: true,
                    contentPadding: EdgeInsets.all(5),
                    trailing: Text(
                      'Вы',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                    subtitle: Text(
                      '${suggestions.comments[index].text}',
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    ),
                  ),
                );
              }
            })
            : Center(child: CircularProgressIndicator()))
        :Expanded(
        child: _userid != null && _userid["id"] != null
            ? ListView.builder(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: comment.comments.length,
            itemBuilder: (context, index) {
              if (_userid["id"] != comment.comments[index].user.id) {
                return Card(
                  color: Colors.white,
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                      bottomLeft: Radius.circular(22.0),
                      topRight: Radius.circular(22.0),
                    ),
                    side: BorderSide(color: Colors.white70, width: 1),
                  ),
                  child: ListTile(
                    minVerticalPadding: 15,
                    minLeadingWidth: 15,
                    //isThreeLine: true,
                    contentPadding: EdgeInsets.all(5),
                    title: Text(
                      '${comment.comments[index].user.fullName}',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                    subtitle: Text(
                      '${comment.comments[index].text}',
                      style: TextStyle(color: Colors.black, fontSize: 17),
                    ),
                  ),
                );
              } else {
                return Card(
                  color: new Color.fromRGBO(32, 86, 146, 1),
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                      bottomLeft: Radius.circular(22.0),
                      topLeft: Radius.circular(22.0),
                    ),
                    side: BorderSide(color: Colors.white70, width: 1),
                  ),
                  child: ListTile(
                    minVerticalPadding: 15,
                    minLeadingWidth: 15,
                    //isThreeLine: true,
                    contentPadding: EdgeInsets.all(5),
                    trailing: Text(
                      'Вы',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                    subtitle: Text(
                      '${comment.comments[index].text}',
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    ),
                  ),
                );
              }
            })
            : Center(child: CircularProgressIndicator()));
  }

  void loadData() {
    getUser().then((response) {
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);
        setState(() {
          _userid = jsonData;
        });
      } else {
        print(response.statusCode);
      }
    });
  }

}*/
