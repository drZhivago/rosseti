import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rosseti_application/futures/project_future.dart';
import 'package:rosseti_application/models/project_model.dart';

import 'project_page.dart';

class ProjectsPage extends StatefulWidget {
  @override
  _ProjectsPage createState() => _ProjectsPage();
}

class _ProjectsPage extends State<ProjectsPage> {
  Future<ProjectModel> projectList;

  @override
  void initState() {
    super.initState();
    projectList = getProject();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Padding(
      padding: EdgeInsets.symmetric(
        vertical: 45,
        horizontal: 8,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.arrow_back_ios_outlined),
                color: new Color.fromRGBO(32, 86, 146, 1),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Text(
                'Проекты',
                style: TextStyle(
                  color: new Color.fromRGBO(32, 86, 146, 1),
                  fontSize: 29,
                ),
              ),
              IconButton(
                icon: Image.asset(
                  'assets/images/image 1.png',
                  width: 150,
                  height: 150,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/userpage');
                },
              ),
            ],
          ),
          FutureBuilder<ProjectModel>(
              future: projectList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Expanded(
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        //scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: snapshot.data.suggestions.length,
                        itemBuilder: (context, index) {
                          return Card(
                            margin: EdgeInsets.all(20),
                            //elevation: 3,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                              side: BorderSide(
                                color: Colors.grey[300],
                                width: 2.0,
                              ),
                            ),
                            child: ListTile(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                    builder: (context) => ProjectPage(
                                        suggestions:
                                            snapshot.data.suggestions[index]));
                                Navigator.push(context, route);
                              },
                              tileColor: Colors.white,
                              contentPadding: EdgeInsets.all(20),
                              //isThreeLine: true,
                              /*title: Text(
                                  '${snapshot.data.suggestions[index].title}'),*/
                              title: snapshot.data.suggestions[index]
                                          .existingSolutionImage !=
                                      null
                                  ? Image.network(
                                      '${snapshot.data.suggestions[index].existingSolutionImage}',
                                      width: 50,
                                      height: 50,
                                    )
                                  : Text(
                                      'Без фото.',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                              subtitle: Text(
                                  'Рейтинг: ${snapshot.data.suggestions[index].rating}'),
                              trailing: Text(
                                '${snapshot.data.suggestions[index].author.fullName}',
                                style: TextStyle(
                                  color: Colors.blue[900],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return Center(child: CircularProgressIndicator());
              }),
        ],
      ),
    )));
  }
}
