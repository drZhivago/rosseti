import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rosseti_application/futures/raiting_future.dart';
import 'package:rosseti_application/models/project_model.dart';
import 'package:rosseti_application/models/raiting_model.dart';
import 'package:video_player/video_player.dart';

import 'comments_page.dart';

class ProjectPage extends StatefulWidget {
  Suggestions suggestions;

  ProjectPage({this.suggestions});

  @override
  _ProjectPage createState() => _ProjectPage(suggestions: suggestions);
}

class _ProjectPage extends State<ProjectPage> {
  Suggestions suggestions;

  _ProjectPage({this.suggestions});

  VideoPlayerController videoController;
  VideoPlayerController secondVideoController;

  int _rating = 0;

  File _firstVideoFile;
  File _secondVideoFile;

  @override
  void initState() {
    loadVideo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 35,
              horizontal: 8,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios_outlined),
                      color: new Color.fromRGBO(32, 86, 146, 1.0),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      "Создать",
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        fontSize: 29,
                      ),
                    ),
                    IconButton(
                      icon: Image.asset(
                        'assets/images/image 1.png',
                        width: 150,
                        height: 150,
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/userpage');
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Сейчас так:",
                  style: TextStyle(
                    color: new Color.fromRGBO(32, 86, 146, 1),
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    suggestions.existingSolutionImage != null
                        ? Image.network(
                            '${suggestions.existingSolutionImage}',
                            width: 120,
                            height: 100,
                            fit: BoxFit.fill,
                          )
                        : Text(
                            'Без фото.',
                            style: TextStyle(color: Colors.grey),
                          ),
                    SizedBox(
                      width: 20,
                    ),
                    suggestions.existingSolutionVideo != null
                        ? Stack(
                            children: [
                              Container(
                                width: 120,
                                height: 100,
                                child: AspectRatio(
                                  aspectRatio: 3 / 2,
                                  child: VideoPlayer(videoController),
                                ),
                              ),
                              Positioned(
                                bottom: 25,
                                right: 25,
                                left: 25,
                                top: 25,
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  child: FloatingActionButton(
                                      heroTag: 'contact4',
                                      backgroundColor: new Color.fromRGBO(
                                          105, 105, 105, 0.7),
                                      onPressed: () {
                                        setState(() {
                                          if (videoController.value.isPlaying)
                                            videoController.pause();
                                          else
                                            videoController.play();
                                        });
                                      }),
                                ),
                              ),
                              Positioned(
                                  bottom: 25,
                                  right: 25,
                                  left: 25,
                                  top: 25,
                                  child: Icon(
                                    videoController.value.isPlaying
                                        ? Icons.pause
                                        : Icons.play_arrow,
                                    color:
                                        new Color.fromRGBO(255, 255, 255, 0.9),
                                  ))
                            ],
                          )
                        : Text(
                            'Без видео.',
                            style: TextStyle(color: Colors.grey),
                          ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: new Color.fromRGBO(255, 255, 255, 1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[100],
                        spreadRadius: 4,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.all(15),
                  width: 200,
                  //color: Colors.white,
                  child: Text(
                    '${suggestions.existingSolutionText}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Надо так:",
                  style: TextStyle(
                    color: new Color.fromRGBO(32, 86, 146, 1),
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    suggestions.proposedSolutionImage != null
                        ? Image.network(
                            '${suggestions.proposedSolutionImage}',
                            width: 120,
                            height: 100,
                            fit: BoxFit.fill,
                          )
                        : Text(
                            'Без фото.',
                            style: TextStyle(color: Colors.grey),
                          ),
                    SizedBox(
                      width: 20,
                    ),
                    suggestions.proposedSolutionVideo != null
                        ? Stack(
                            children: [
                              Container(
                                width: 120,
                                height: 100,
                                child: AspectRatio(
                                  aspectRatio: 3 / 2,
                                  child: VideoPlayer(secondVideoController),
                                ),
                              ),
                              Positioned(
                                bottom: 25,
                                right: 25,
                                left: 25,
                                top: 25,
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  child: FloatingActionButton(
                                      heroTag: 'contact3',
                                      backgroundColor: new Color.fromRGBO(
                                          105, 105, 105, 0.7),
                                      onPressed: () {
                                        setState(() {
                                          if (secondVideoController
                                              .value.isPlaying)
                                            secondVideoController.pause();
                                          else
                                            secondVideoController.play();
                                        });
                                      }),
                                ),
                              ),
                              Positioned(
                                  bottom: 25,
                                  right: 25,
                                  left: 25,
                                  top: 25,
                                  child: Icon(
                                    secondVideoController.value.isPlaying
                                        ? Icons.pause
                                        : Icons.play_arrow,
                                    color:
                                        new Color.fromRGBO(255, 255, 255, 0.9),
                                  ))
                            ],
                          )
                        : Text(
                            'Без видео.',
                            style: TextStyle(color: Colors.grey),
                          ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: new Color.fromRGBO(255, 255, 255, 1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[100],
                        spreadRadius: 4,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.all(15),
                  width: 200,
                  child: Text(
                    '${suggestions.proposedSolutionText}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "И тогда будет так:",
                  style: TextStyle(
                    color: new Color.fromRGBO(32, 86, 146, 1),
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: new Color.fromRGBO(255, 255, 255, 1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[100],
                        spreadRadius: 4,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.all(15),
                  width: 200,
                  child: Text(
                    '${suggestions.positiveEffect}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Оцените проект:",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: IconButton(
                        icon: (_rating >= 1
                            ? Icon(
                                Icons.star,
                                size: 50,
                                color: new Color.fromRGBO(225, 206, 71, 1),
                              )
                            : Icon(
                                Icons.star,
                                size: 50,
                                color: Colors.grey,
                              )),
                        onPressed: () {
                          setState(() {
                            _rating = 1;
                          });
                        },
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: (_rating >= 2
                            ? Icon(
                                Icons.star,
                                size: 50,
                                color: new Color.fromRGBO(225, 206, 71, 1),
                              )
                            : Icon(
                                Icons.star,
                                size: 50,
                                color: Colors.grey,
                              )),
                        onPressed: () {
                          setState(() {
                            _rating = 2;
                          });
                        },
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: (_rating >= 3
                            ? Icon(
                                Icons.star,
                                size: 50,
                                color: new Color.fromRGBO(225, 206, 71, 1),
                              )
                            : Icon(
                                Icons.star,
                                size: 50,
                                color: Colors.grey,
                              )),
                        onPressed: () {
                          setState(() {
                            _rating = 3;
                          });
                        },
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: (_rating >= 4
                            ? Icon(
                                Icons.star,
                                size: 50,
                                color: new Color.fromRGBO(225, 206, 71, 1),
                              )
                            : Icon(
                                Icons.star,
                                size: 50,
                                color: Colors.grey,
                              )),
                        onPressed: () {
                          setState(() {
                            _rating = 4;
                          });
                        },
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: (_rating >= 5
                            ? Icon(
                                Icons.star,
                                size: 50,
                                color: new Color.fromRGBO(225, 206, 71, 1),
                              )
                            : Icon(
                                Icons.star,
                                size: 50,
                                color: Colors.grey,
                              )),
                        onPressed: () {
                          setState(() {
                            _rating = 5;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 130,
                      height: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: new Color.fromRGBO(32, 86, 146, 0.9),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[100],
                            spreadRadius: 4,
                            blurRadius: 5,
                            offset: Offset(0, 5),
                          ),
                        ],
                      ),
                      child: IconButton(
                        icon: (Icon(
                          Icons.add,
                          color: Colors.white,
                        )),
                        onPressed: () async {
                          final RatingModel raiting = await addrating(
                            suggestions.id.toString(),
                            _rating.toString(),
                          );
                          Navigator.pop(context);
                        },
                        iconSize: 50,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Container(
                      width: 130,
                      height: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[100],
                            spreadRadius: 4,
                            blurRadius: 5,
                            offset: Offset(0, 5),
                          ),
                        ],
                      ),
                      child: IconButton(
                        icon: (Icon(
                          Icons.clear,
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        )),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        iconSize: 50,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: 300,
                  child: FloatingActionButton.extended(
                    elevation: 3,
                    onPressed: () {
                      Route route = MaterialPageRoute(
                          builder: (context) =>
                              CommentsPage(suggestions: suggestions));
                      Navigator.push(context, route);
                    },
                    label: Text(
                      'Обсудить',
                      style: TextStyle(
                        fontSize: 20,
                        color: new Color.fromRGBO(32, 86, 146, 0.8),
                      ),
                    ),
                    backgroundColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void loadVideo() {
    if (suggestions.existingSolutionVideo != null) {
      _firstVideoFile = File(suggestions.proposedSolutionVideo);
      videoController = VideoPlayerController.file(_firstVideoFile)
        ..initialize().then((_) {
          setState(() {});
          videoController.setLooping(true);
        });
    } else
      null;

    if (suggestions.proposedSolutionVideo != null) {
      _secondVideoFile = File(suggestions.proposedSolutionVideo);
      secondVideoController = VideoPlayerController.file(_secondVideoFile)
        ..initialize().then((_) {
          setState(() {});
          secondVideoController.setLooping(true);
        });
    } else
      null;
  }
}
