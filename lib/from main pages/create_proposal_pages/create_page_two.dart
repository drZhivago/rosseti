//import 'dart:io';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'create_page_three.dart';
import 'proposal_class.dart';

import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

class CreatePage2 extends StatefulWidget {
  Proposal prop;

  CreatePage2({this.prop});

  @override
  _CreatePage2 createState() => _CreatePage2(prop: prop);
}

class _CreatePage2 extends State<CreatePage2> {
  Proposal prop;

  _CreatePage2({this.prop});

  final TextEditingController storyController = TextEditingController();
  VideoPlayerController videoController;

  PickedFile _imageFile;
  File _videoFile;

  void takePhoto(ImageSource source) async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  getVideo() async {
    final _pickerVideo = ImagePicker();
    PickedFile pickedVideo =
        await _pickerVideo.getVideo(source: ImageSource.gallery);
    _videoFile = File(pickedVideo.path);
    videoController = VideoPlayerController.file(_videoFile)
      ..initialize().then((_) {
        setState(() {});
        videoController.setLooping(true);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 35,
              horizontal: 8,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios_outlined),
                      color: new Color.fromRGBO(32, 86, 146, 1.0),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      "Создать",
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        fontSize: 29,
                      ),
                    ),
                    IconButton(
                      icon: Image.asset(
                        'assets/images/image 1.png',
                        width: 150,
                        height: 150,
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/userpage');
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  'Расскажите как сейчас',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 300,
                  child: TextFormField(
                    controller: storyController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(
                            color: new Color.fromRGBO(32, 86, 146, 1),
                          )),
                      filled: true,
                    ),
                    maxLines: 10,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Добавьте фото или видео',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _imageFile == null
                        ? SizedBox()
                        : Image(
                            fit: BoxFit.fill,
                            width: 120,
                            height: 100,
                            image: FileImage(File(_imageFile.path)),
                          ),
                    SizedBox(
                      width: 25,
                    ),
                    if (_videoFile != null)
                      videoController.value.isInitialized
                          ? Stack(
                              children: [
                                Container(
                                  width: 120,
                                  height: 100,
                                  child: AspectRatio(
                                    aspectRatio: 3 / 2,
                                    child: VideoPlayer(videoController),
                                  ),
                                ),
                                Positioned(
                                  bottom: 25,
                                  right: 25,
                                  left: 25,
                                  top: 25,
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    child: FloatingActionButton(

                                        ///выдаёт ошибку, если не указываю тег
                                        heroTag: 'contact',
                                        backgroundColor: new Color.fromRGBO(
                                            105, 105, 105, 0.7),
                                        onPressed: () {
                                          setState(() {
                                            if (videoController.value.isPlaying)
                                              videoController.pause();
                                            else
                                              videoController.play();
                                          });
                                        }),
                                  ),
                                ),
                                Positioned(
                                    bottom: 25,
                                    right: 25,
                                    left: 25,
                                    top: 25,
                                    child: Icon(
                                      videoController.value.isPlaying
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                      color: new Color.fromRGBO(
                                          255, 255, 255, 0.9),
                                    ))
                              ],
                            )
                          : SizedBox(),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        icon: Icon(Icons.camera_enhance_outlined),
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        onPressed: () {
                          takePhoto(ImageSource.gallery);
                        }),
                    SizedBox(
                      width: 25,
                    ),
                    IconButton(
                        icon: Icon(Icons.videocam_outlined),
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        onPressed: () {
                          getVideo();
                        }),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: 300,
                  child: FloatingActionButton.extended(
                    elevation: 3,
                    onPressed: () {
                      prop.existing_solution_text = storyController.text;
                      prop.existing_solution_image = _imageFile.path;
                      prop.existing_solution_video = _videoFile.path;
                      Route route = MaterialPageRoute(
                          builder: (context) => CreatePage3(prop: prop));
                      Navigator.push(context, route);
                    },
                    label: Text(
                      'Дальше',
                      style: TextStyle(
                        fontSize: 20,
                        color: new Color.fromRGBO(32, 86, 146, 0.8),
                      ),
                    ),
                    backgroundColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    videoController.dispose();
  }
}
