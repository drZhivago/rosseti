import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rosseti_application/from%20main%20pages/create_proposal_pages/create_page_two.dart';
import 'package:rosseti_application/futures/topics_future.dart';
import 'package:rosseti_application/models/topic_mpdel.dart';

import 'proposal_class.dart';

class CreatePage extends StatefulWidget {
  @override
  _CreatePage createState() => _CreatePage();
}

class _CreatePage extends State<CreatePage> {
  final TextEditingController themeController = TextEditingController();
  final TextEditingController nameController = TextEditingController();

  void initState() {
    loadData();
    super.initState();
  }

  var _postJson;

  int topic_id;

  String selecttopic;

  List<TopicModel> topicList;

  Future<TopicModel> topFuture;

  TopicModel topModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///SingleChildScrollView позволяет избежать проблему с переполненными пикселями при выходе клавиатуры
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 45,
              horizontal: 8,
            ),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios_outlined),
                    color: new Color.fromRGBO(32, 86, 146, 1),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Text(
                    'Создать',
                    style: TextStyle(
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      fontSize: 29,
                    ),
                  ),
                  IconButton(
                    icon: Image.asset(
                      'assets/images/image 1.png',
                      width: 150,
                      height: 150,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/userpage');
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'Расскажите о предложении',
                style: TextStyle(
                  fontSize: 20,
                  color: new Color.fromRGBO(32, 86, 146, 1),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'Выберите тему и название',
                style: TextStyle(
                  fontSize: 20,
                  color: new Color.fromRGBO(32, 86, 146, 1),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: 300,
                child: DropdownButtonFormField<String>(
                  isDense: false,
                  isExpanded: true,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    labelText: 'Тема проекта',
                    labelStyle: TextStyle(fontSize: 20),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: BorderSide(
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        )),
                  ),
                  items: _postJson != null && _postJson["topics"] != null
                      ? _postJson["topics"]
                          .map<DropdownMenuItem<String>>((topic) {
                          return DropdownMenuItem<String>(
                            child: Text(
                              topic["title"],
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                            value: topic["title"],
                            onTap: () {
                              topic_id = topic["id"];
                            },
                          );
                        }).toList()
                      : null,
                  onChanged: (topic) {
                    setState(() {
                      selecttopic = topic;
                    });
                  },
                  value: selecttopic,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: 300,
                child: TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    labelText: 'Название',
                    labelStyle: TextStyle(fontSize: 20),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: BorderSide(
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        )),
                    //filled: true,
                  ),
                ),
              ),
              SizedBox(
                height: 100,
              ),
              SizedBox(
                width: 300,
                child: FloatingActionButton.extended(
                  elevation: 3,
                  onPressed: () {
                    senddata();
                  },
                  label: Text(
                    'Дальше',
                    style: TextStyle(
                      fontSize: 20,
                      color: new Color.fromRGBO(32, 86, 146, 0.8),
                    ),
                  ),
                  backgroundColor: Colors.white,
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  void senddata() {
    Proposal prop =
        Proposal(name: nameController.text, topic_id: topic_id.toString());
    Route route =
        MaterialPageRoute(builder: (context) => CreatePage2(prop: prop));
    Navigator.push(context, route);
  }

  void loadData() {
    gettopic().then((response) {
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(response.body);

        setState(() {
          _postJson = jsonData;
        });
      } else {
        print(response.statusCode);
      }
    });
  }
}
