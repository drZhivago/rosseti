import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

import 'create_page_four.dart';
import 'proposal_class.dart';

class CreatePage3 extends StatefulWidget {
  Proposal prop;

  CreatePage3({this.prop});

  @override
  _CreatePage3 createState() => _CreatePage3(prop: prop);
}

class _CreatePage3 extends State<CreatePage3> {
  Proposal prop;

  _CreatePage3({this.prop});

  final TextEditingController storyController = TextEditingController();
  VideoPlayerController videoController;

  PickedFile _imageFile;
  File _videoFile;

  final ImagePicker _picker = ImagePicker();

  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  getVideo() async {
    final _pickerVideo = ImagePicker();
    PickedFile pickedVideo =
        await _pickerVideo.getVideo(source: ImageSource.gallery);
    _videoFile = File(pickedVideo.path);
    videoController = VideoPlayerController.file(_videoFile)
      ..initialize().then((_) {
        setState(() {});
        videoController.setLooping(true);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 35,
              horizontal: 8,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios_outlined),
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      'Создать',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        fontSize: 29,
                      ),
                    ),
                    IconButton(
                      icon: Image.asset(
                        'assets/images/image 1.png',
                        width: 150,
                        height: 150,
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/userpage');
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  'Расскажите как надо',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 300,
                  child: TextFormField(
                    controller: storyController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(
                            color: new Color.fromRGBO(32, 86, 146, 1),
                          )),
                      filled: true,
                    ),
                    maxLines: 10,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Добавьте фото или видео',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _imageFile == null
                        ? SizedBox()
                        : Image(
                            fit: BoxFit.fill,
                            width: 120,
                            height: 100,
                            image: FileImage(File(_imageFile.path)),
                          ),
                    SizedBox(
                      width: 25,
                    ),
                    if (_videoFile != null)
                      videoController.value.isInitialized
                          ? Stack(
                              children: [
                                Container(
                                  width: 120,
                                  height: 100,
                                  child: AspectRatio(
                                    aspectRatio: 3 / 2,
                                    child: VideoPlayer(videoController),
                                  ),
                                ),
                                Positioned(
                                  bottom: 25,
                                  right: 25,
                                  left: 25,
                                  top: 25,
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    child: FloatingActionButton(
                                        heroTag: 'contact2',
                                        backgroundColor: new Color.fromRGBO(
                                            105, 105, 105, 0.7),
                                        onPressed: () {
                                          setState(() {
                                            if (videoController.value.isPlaying)
                                              videoController.pause();
                                            else
                                              videoController.play();
                                          });
                                        }),
                                  ),
                                ),
                                Positioned(
                                    bottom: 25,
                                    right: 25,
                                    left: 25,
                                    top: 25,
                                    child: Icon(
                                      videoController.value.isPlaying
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                      color: new Color.fromRGBO(
                                          255, 255, 255, 0.9),
                                    ))
                              ],
                            )
                          : SizedBox(),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        icon: Icon(Icons.camera_enhance_outlined),
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        onPressed: () {
                          takePhoto(ImageSource.gallery);
                        }),
                    SizedBox(
                      width: 25,
                    ),
                    IconButton(
                        icon: Icon(Icons.videocam_outlined),
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        onPressed: () {
                          getVideo();
                        }),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: 300,
                  child: FloatingActionButton.extended(
                    elevation: 3,
                    onPressed: () {
                      prop.proposed_solution_text = storyController.text;
                      prop.proposed_solution_image = _imageFile.path;
                      prop.proposed_solution_video = _videoFile.path;
                      Route route = MaterialPageRoute(
                          builder: (context) => CreatePage4(prop: prop));
                      Navigator.push(context, route);
                    },
                    label: Text(
                      'Дальше',
                      style: TextStyle(
                        fontSize: 20,
                        color: new Color.fromRGBO(32, 86, 146, 0.8),
                      ),
                    ),
                    backgroundColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    videoController.dispose();
  }
}
