import 'package:flutter/material.dart';
import 'package:rosseti_application/from%20main%20pages/create_proposal_pages/proposal_class.dart';
import 'package:rosseti_application/models/offer_model.dart';

import 'package:http/http.dart' as http;
import 'package:rosseti_application/utils/settings.dart';

class CreatePage4 extends StatefulWidget {
  Proposal prop;

  CreatePage4({this.prop});

  @override
  _CreatePage4 createState() => _CreatePage4(prop: prop);
}

class _CreatePage4 extends State<CreatePage4> {
  Proposal prop;

  _CreatePage4({this.prop});

  final TextEditingController storyController = TextEditingController();

  final String offerurl =
      'https://msofter.com/rosseti/public/api/suggestions/store';

  Future<String> ican(
    topic_id,
    name,
    existing_solution_text,
    existing_solution_image,
    existing_solution_video,
    proposed_solution_text,
    proposed_solution_image,
    proposed_solution_video,
    positive_effect,
  ) async {
    var request = http.MultipartRequest('POST', Uri.parse(offerurl));
    request.headers['Accept'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ' + Settings.token;
    //request.headers['Authorization'] =
    //    'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2E1OGE4ZmFiMWY4YjgxZGM0YzIwYjczNzExY2Y2NjFiZWE0ZjkwMzI4MTE5OTJiZjUxODVkZGZkODQ0NzNjMmZiY2Q5MTJjZWQxZmMyNmUiLCJpYXQiOjE2MDY1NjI5ODUsIm5iZiI6MTYwNjU2Mjk4NSwiZXhwIjoxNjM4MDk4OTg1LCJzdWIiOiIyMiIsInNjb3BlcyI6W119.j6-1q2IXESdbmDJGex0rayOC-UnfRR9yWi2Xd341Jlt2fH-Dm2csZFDsxLdTi3QeuvCIgwOkBMJFo2dFhhTgY2Bt6jBbmUHoUqYTFR3bVi6aZybsbPCA15c6_3K7rWUbhXYoc1MaiBVhNa28XRbydWLwcylORI673wJ2Ovnt5w12PMFGxjNdP145_rg-7cuax_cfZF27aI-01VeyDx-SNCb4LpNYvi4EW3r6mGCTqruIzW8F_fyq56wF41jxgtDYs2kvHb4beMgHjkdtY3yi26hoAksN1vfBdx3gD-pDHIvSJDDy2Mb4q_XqLr6t-MYeQalg-8sLog4gnrVt2Pa7isYVKD8r7k9IHhuyEA145y-dBL5Xz8h1hSrdatDN00PxqAdkHkylkM8e-w6vF0m_xIlt8HkOuz1gLbWBnO1kNr7S1GSR_13P0oOa9tl3glb1ccYmcUTzveu58t3o0iz24aXrKin19AXDFujGntWE1RIyIJKfs3FbQ3EvX6W80hXRHkazfVHLsok9DZCzMt_P8vCacFAuDUO9hsShV9JIG1oMGYrYubepw3f6X_8RWVIAxy8TBz9ZuUogrszmT_ks6boYkXFsb2tJ2QkdnpZP8foImpzMoVOHkxrygYlqr4Ta90YGFswCq7PIg8uldB-RWS3RgcNnmiU7BmIx8plpibk';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.fields['topic_id'] = prop.topic_id;
    request.fields['name'] = prop.name;
    request.fields['existing_solution_text'] = prop.existing_solution_text;
    request.files.add(await http.MultipartFile.fromPath(
        "existing_solution_image", prop.existing_solution_image));
    request.files.add(await http.MultipartFile.fromPath(
        "existing_solution_video", prop.existing_solution_video));
    request.fields['proposed_solution_text'] = prop.proposed_solution_text;
    request.files.add(await http.MultipartFile.fromPath(
        "proposed_solution_image", prop.proposed_solution_image));
    request.files.add(await http.MultipartFile.fromPath(
        "proposed_solution_video", prop.proposed_solution_video));
    request.fields['positive_effect'] = prop.positive_effect;

    var res = await request.send();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 45,
              horizontal: 8,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios_outlined),
                      color: new Color.fromRGBO(32, 86, 146, 1),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      'Создать',
                      style: TextStyle(
                        color: new Color.fromRGBO(32, 86, 146, 1),
                        fontSize: 29,
                      ),
                    ),
                    IconButton(
                      icon: Image.asset(
                        'assets/images/image 1.png',
                        width: 150,
                        height: 150,
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/userpage');
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Расскажите как будет',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  width: 300,
                  child: TextFormField(
                    controller: storyController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(
                            color: new Color.fromRGBO(32, 86, 146, 1),
                          )),
                      filled: true,
                    ),
                    maxLines: 10,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 300,
                  child: FloatingActionButton.extended(
                    elevation: 3,
                    onPressed: () async {
                      prop.positive_effect = storyController.text;
                      var res = await ican(
                        prop.topic_id,
                        prop.name,
                        prop.existing_solution_text,
                        prop.existing_solution_image,
                        prop.existing_solution_video,
                        prop.proposed_solution_text,
                        prop.proposed_solution_image,
                        prop.proposed_solution_video,
                        prop.positive_effect,
                      );
                      Navigator.pushNamed(context, '/mainpage');
                    },
                    label: Text(
                      'Готово',
                      style: TextStyle(
                        fontSize: 20,
                        color: new Color.fromRGBO(32, 86, 146, 0.8),
                      ),
                    ),
                    backgroundColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
