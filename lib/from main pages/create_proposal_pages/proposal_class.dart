import 'package:image_picker/image_picker.dart';

class Proposal {
  String topic_id;
  String name;
  String existing_solution_text;
  String existing_solution_image;
  String existing_solution_video;
  String proposed_solution_text;
  String proposed_solution_image;
  String proposed_solution_video;
  String positive_effect;

  Proposal({
    this.topic_id,
    this.name,
    this.existing_solution_text,
    this.existing_solution_image,
    this.existing_solution_video,
    this.proposed_solution_text,
    this.proposed_solution_image,
    this.proposed_solution_video,
    this.positive_effect,
  });
}
