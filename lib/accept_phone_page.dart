import 'package:flutter/material.dart';

import 'package:rosseti_application/futures/accept_future.dart';
import 'package:rosseti_application/main_page.dart';

import 'package:rosseti_application/models/verify_model.dart';
import 'package:rosseti_application/utils/settings.dart';

class AcceptPage extends StatefulWidget {
  @override
  _AcceptPage createState() => _AcceptPage();
}

class _AcceptPage extends State<AcceptPage> {
  final TextEditingController codeController = TextEditingController();

  bool isValidate = false;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _formKey,
              child: ListView(
                /*scrollDirection: Axis.vertical,*/
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(30, 0, 30, 16),
                children: [
                  TextFormField(
                    controller: codeController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      hintText: 'Код из смс',
                      labelStyle: TextStyle(fontSize: 20),
                      prefixIcon: Icon(
                        Icons.phone,
                        color: new Color.fromRGBO(32, 86, 146, 0.8),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(
                            color: new Color.fromRGBO(32, 86, 146, 1),
                          )),
                      filled: true,
                    ),
                    keyboardType: TextInputType.phone,
                    validator: (val) => val.isEmpty ? 'Введите код' : null,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 300,
              child: FloatingActionButton.extended(
                elevation: 3,
                onPressed: () async {
                  _validTel();
                  if (isValidate) {
                    final VerifyModel veri =
                        await verifycode(codeController.text);
                    //prefs.remove("stringValue");
                    Settings.setToken(veri.token);
                    Route route =
                        MaterialPageRoute(builder: (context) => MainPage());
                    Navigator.push(context, route);
                  }
                },
                label: Text(
                  'Готово',
                  style: TextStyle(
                    fontSize: 20,
                    color: new Color.fromRGBO(32, 86, 146, 0.8),
                  ),
                ),
                backgroundColor: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _validTel() {
    if (_formKey.currentState.validate()) {
      isValidate = true;
    } else {
      isValidate = false;
    }
  }
}
