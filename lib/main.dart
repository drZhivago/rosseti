import 'package:flutter/material.dart';

import 'from main pages/create_proposal_pages/create_page.dart';
import 'from main pages/projects/projects.dart';
import 'front_page.dart';
import 'main_page.dart';
import 'from main pages/user_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: new Color.fromRGBO(250, 251, 253, 1),
      ),
      home: FrontPage()  /*MainPage()*/,
      routes: {
        '/createpage': (context) => CreatePage(),
        '/projectpage': (context) => ProjectsPage(),
        '/projectpage2': (context) => ProjectsPage(),
        '/userpage': (context) => UserPage(),
        '/mainpage': (context) => MainPage(),
      },
    );
  }
}
