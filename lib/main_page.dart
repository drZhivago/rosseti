import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage> {
  int numberpage;

  void gotopages() {
    if (numberpage == 1) Navigator.pushNamed(context, '/createpage');
    if (numberpage == 2) Navigator.pushNamed(context, '/projectpage');
    if (numberpage == 3) Navigator.pushNamed(context, '/projectpage2');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 70,
                ),
                Text(
                  'seti.inno',
                  style: TextStyle(
                    fontSize: 28,
                    color: new Color.fromRGBO(32, 86, 146, 1),
                    fontFamily: 'JosefinSans',
                  ),
                ),
                IconButton(
                  icon: Image.asset(
                    'assets/images/image 1.png',
                    width: 150,
                    height: 150,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/userpage');
                  },
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),

            ///первая штучка
            InkWell(
              onTap: () {
                numberpage = 1;
                gotopages();
              },
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: new Color.fromRGBO(255, 255, 255, 1),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: Offset(0, 5),
                    ),
                  ],
                ),
                width: 220,
                height: 100,
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/create 1.png',
                      width: 90,
                      height: 90,
                    ),
                    Expanded(
                      child: Text(
                        'Создать предложение',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),

            ///второя штучка
            InkWell(
              onTap: () {
                numberpage = 2;
                gotopages();
              },
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: new Color.fromRGBO(255, 255, 255, 1),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: Offset(0, 5),
                    ),
                  ],
                ),
                width: 220,
                height: 100,
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/idea 1.png',
                      width: 90,
                      height: 90,
                    ),
                    Expanded(
                      child: Text(
                        'Заявки',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),

            ///третья штучка
            InkWell(
              onTap: () {
                numberpage = 3;
                gotopages();
              },
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: new Color.fromRGBO(255, 255, 255, 1),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: Offset(0, 5),
                    ),
                  ],
                ),
                width: 220,
                height: 100,
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/skills 1.png',
                      width: 90,
                      height: 90,
                    ),
                    Expanded(
                      child: Text(
                        'Экспертизы',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          color: new Color.fromRGBO(32, 86, 146, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
