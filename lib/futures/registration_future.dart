import 'dart:convert';

import 'package:rosseti_application/models/phone_model.dart';

import 'package:http/http.dart' as http;

Future<PhoneModel> authphone(String phone) async {
  final String authurl = 'https://msofter.com/rosseti/public/api/auth/phone';
  final response = await http.post(Uri.parse(authurl), body: {
    "phone": phone,
  });

  if (response.statusCode == 200) {
    final String responseString = response.body;
    return PhoneModel.fromJson(json.decode(responseString));
  } else {
    return null;
  }
}
