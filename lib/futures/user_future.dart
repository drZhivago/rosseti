import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:http/http.dart' as http;

import 'package:rosseti_application/accept_phone_page.dart';
import 'package:rosseti_application/utils/settings.dart';

Future<http.Response> getUser() async {
  const userurl = 'https://msofter.com/rosseti/public/api/user';
  final result = await http.get(Uri.parse(userurl), headers: {
    "Accept": "application/json",
    //"Authorization":
    //    'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2E1OGE4ZmFiMWY4YjgxZGM0YzIwYjczNzExY2Y2NjFiZWE0ZjkwMzI4MTE5OTJiZjUxODVkZGZkODQ0NzNjMmZiY2Q5MTJjZWQxZmMyNmUiLCJpYXQiOjE2MDY1NjI5ODUsIm5iZiI6MTYwNjU2Mjk4NSwiZXhwIjoxNjM4MDk4OTg1LCJzdWIiOiIyMiIsInNjb3BlcyI6W119.j6-1q2IXESdbmDJGex0rayOC-UnfRR9yWi2Xd341Jlt2fH-Dm2csZFDsxLdTi3QeuvCIgwOkBMJFo2dFhhTgY2Bt6jBbmUHoUqYTFR3bVi6aZybsbPCA15c6_3K7rWUbhXYoc1MaiBVhNa28XRbydWLwcylORI673wJ2Ovnt5w12PMFGxjNdP145_rg-7cuax_cfZF27aI-01VeyDx-SNCb4LpNYvi4EW3r6mGCTqruIzW8F_fyq56wF41jxgtDYs2kvHb4beMgHjkdtY3yi26hoAksN1vfBdx3gD-pDHIvSJDDy2Mb4q_XqLr6t-MYeQalg-8sLog4gnrVt2Pa7isYVKD8r7k9IHhuyEA145y-dBL5Xz8h1hSrdatDN00PxqAdkHkylkM8e-w6vF0m_xIlt8HkOuz1gLbWBnO1kNr7S1GSR_13P0oOa9tl3glb1ccYmcUTzveu58t3o0iz24aXrKin19AXDFujGntWE1RIyIJKfs3FbQ3EvX6W80hXRHkazfVHLsok9DZCzMt_P8vCacFAuDUO9hsShV9JIG1oMGYrYubepw3f6X_8RWVIAxy8TBz9ZuUogrszmT_ks6boYkXFsb2tJ2QkdnpZP8foImpzMoVOHkxrygYlqr4Ta90YGFswCq7PIg8uldB-RWS3RgcNnmiU7BmIx8plpibk',
    "Authorization":'Bearer ' + Settings.token,
  });
  return result;
}
