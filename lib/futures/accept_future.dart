import 'dart:convert';

import 'package:rosseti_application/models/verify_model.dart';

import 'package:http/http.dart' as http;

Future<VerifyModel> verifycode(String code) async {
  final String veriurl =
      'https://msofter.com/rosseti/public/api/auth/verify-code';

  final response = await http.post(Uri.parse(veriurl), body: {
    "code": code,
  });

  if (response.statusCode == 200) {
    final String responseString = response.body;
    return VerifyModel.fromJson(json.decode(responseString));
  } else {
    return null;
  }
}
