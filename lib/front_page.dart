import 'package:flutter/material.dart';

import 'package:rosseti_application/registration_page.dart';

class FrontPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/image 1.png',
              width: 150,
              height: 150,
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'seti.inno',
              style: TextStyle(
                fontSize: 40,
                color: new Color.fromRGBO(32, 86, 146, 1),
                fontFamily: 'JosefinSans',
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              'Рационализатор',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 200,
            ),
            Align(
              alignment: Alignment(0, -1.6),
              child: SizedBox(
                width: 300,
                child: FloatingActionButton.extended(
                  elevation: 3,
                  onPressed: () {
                    Route route = MaterialPageRoute(
                        builder: (context) => RegistrationPage());
                    Navigator.push(context, route);
                  },
                  label: Text(
                    'Регистрация',
                    style: TextStyle(
                      fontSize: 20,
                      color: new Color.fromRGBO(32, 86, 146, 0.8),
                    ),
                  ),
                  backgroundColor: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
