class PhoneModel {
  bool success;
  String code;

  PhoneModel({this.success, this.code});

  PhoneModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['code'] = this.code;
    return data;
  }
}