class CommentModel {
  bool success;
  List<Comments> comments;

  CommentModel({this.success, this.comments});

  CommentModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comments {
  int id;
  String text;
  int userId;
  int suggestionId;
  String datetime;
  int you;
  User user;

  Comments(
      {this.id,
        this.text,
        this.userId,
        this.suggestionId,
        this.datetime,
        this.you,
        this.user});

  Comments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    userId = json['user_id'];
    suggestionId = json['suggestion_id'];
    datetime = json['datetime'];
    you = json['you'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['user_id'] = this.userId;
    data['suggestion_id'] = this.suggestionId;
    data['datetime'] = this.datetime;
    data['you'] = this.you;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  int id;
  String fullName;
  String phone;
  int topicId;
  String email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  User(
      {this.id,
        this.fullName,
        this.phone,
        this.topicId,
        this.email,
        this.commentsCount,
        this.ratingsCount,
        this.acceptedProposalsCount,
        this.deniedProposalsCount,
        this.proposalsCount});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    topicId = json['topic_id'];
    email = json['email'];
    commentsCount = json['comments_count'];
    ratingsCount = json['ratings_count'];
    acceptedProposalsCount = json['accepted_proposals_count'];
    deniedProposalsCount = json['denied_proposals_count'];
    proposalsCount = json['proposals_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['topic_id'] = this.topicId;
    data['email'] = this.email;
    data['comments_count'] = this.commentsCount;
    data['ratings_count'] = this.ratingsCount;
    data['accepted_proposals_count'] = this.acceptedProposalsCount;
    data['denied_proposals_count'] = this.deniedProposalsCount;
    data['proposals_count'] = this.proposalsCount;
    return data;
  }
}