class ProjectModel {
  bool success;
  List<Suggestions> suggestions;

  ProjectModel({this.success, this.suggestions});

  ProjectModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['suggestions'] != null) {
      suggestions = new List<Suggestions>();
      json['suggestions'].forEach((v) {
        suggestions.add(new Suggestions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.suggestions != null) {
      data['suggestions'] = this.suggestions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Suggestions {
  int id;
  int authorId;
  String title;
  int topicId;
  String existingSolutionText;
  String existingSolutionImage;
  String existingSolutionVideo;
  String proposedSolutionText;
  String proposedSolutionImage;
  String proposedSolutionVideo;
  String positiveEffect;
  int statusId;
  int registrationNumber;
  int rating;
  int experted;
  Author author;
  List<Comments> comments;

  Suggestions(
      {this.id,
      this.authorId,
      this.title,
      this.topicId,
      this.existingSolutionText,
      this.existingSolutionImage,
      this.existingSolutionVideo,
      this.proposedSolutionText,
      this.proposedSolutionImage,
      this.proposedSolutionVideo,
      this.positiveEffect,
      this.statusId,
      this.registrationNumber,
      this.rating,
      this.experted,
      this.author,
      this.comments});

  Suggestions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    authorId = json['author_id'];
    title = json['title'];
    topicId = json['topic_id'];
    existingSolutionText = json['existing_solution_text'];
    existingSolutionImage = json['existing_solution_image'];
    existingSolutionVideo = json['existing_solution_video'];
    proposedSolutionText = json['proposed_solution_text'];
    proposedSolutionImage = json['proposed_solution_image'];
    proposedSolutionVideo = json['proposed_solution_video'];
    positiveEffect = json['positive_effect'];
    statusId = json['status_id'];
    registrationNumber = json['registration_number'];
    rating = json['rating'];
    experted = json['experted'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['author_id'] = this.authorId;
    data['title'] = this.title;
    data['topic_id'] = this.topicId;
    data['existing_solution_text'] = this.existingSolutionText;
    data['existing_solution_image'] = this.existingSolutionImage;
    data['existing_solution_video'] = this.existingSolutionVideo;
    data['proposed_solution_text'] = this.proposedSolutionText;
    data['proposed_solution_image'] = this.proposedSolutionImage;
    data['proposed_solution_video'] = this.proposedSolutionVideo;
    data['positive_effect'] = this.positiveEffect;
    data['status_id'] = this.statusId;
    data['registration_number'] = this.registrationNumber;
    data['rating'] = this.rating;
    data['experted'] = this.experted;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Author {
  int id;
  String fullName;
  String phone;
  int topicId;
  String email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  Author(
      {this.id,
      this.fullName,
      this.phone,
      this.topicId,
      this.email,
      this.commentsCount,
      this.ratingsCount,
      this.acceptedProposalsCount,
      this.deniedProposalsCount,
      this.proposalsCount});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    topicId = json['topic_id'];
    email = json['email'];
    commentsCount = json['comments_count'];
    ratingsCount = json['ratings_count'];
    acceptedProposalsCount = json['accepted_proposals_count'];
    deniedProposalsCount = json['denied_proposals_count'];
    proposalsCount = json['proposals_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['topic_id'] = this.topicId;
    data['email'] = this.email;
    data['comments_count'] = this.commentsCount;
    data['ratings_count'] = this.ratingsCount;
    data['accepted_proposals_count'] = this.acceptedProposalsCount;
    data['denied_proposals_count'] = this.deniedProposalsCount;
    data['proposals_count'] = this.proposalsCount;
    return data;
  }
}

class Comments {
  int id;
  String text;
  int userId;
  int suggestionId;
  String datetime;
  int you;
  User user;

  Comments(
      {this.id,
      this.text,
      this.userId,
      this.suggestionId,
      this.datetime,
      this.you,
      this.user});

  Comments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    userId = json['user_id'];
    suggestionId = json['suggestion_id'];
    datetime = json['datetime'];
    you = json['you'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['user_id'] = this.userId;
    data['suggestion_id'] = this.suggestionId;
    data['datetime'] = this.datetime;
    data['you'] = this.you;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  int id;
  String fullName;
  String phone;
  int topicId;
  String email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  User(
      {this.id,
        this.fullName,
        this.phone,
        this.topicId,
        this.email,
        this.commentsCount,
        this.ratingsCount,
        this.acceptedProposalsCount,
        this.deniedProposalsCount,
        this.proposalsCount});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    topicId = json['topic_id'];
    email = json['email'];
    commentsCount = json['comments_count'];
    ratingsCount = json['ratings_count'];
    acceptedProposalsCount = json['accepted_proposals_count'];
    deniedProposalsCount = json['denied_proposals_count'];
    proposalsCount = json['proposals_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['topic_id'] = this.topicId;
    data['email'] = this.email;
    data['comments_count'] = this.commentsCount;
    data['ratings_count'] = this.ratingsCount;
    data['accepted_proposals_count'] = this.acceptedProposalsCount;
    data['denied_proposals_count'] = this.deniedProposalsCount;
    data['proposals_count'] = this.proposalsCount;
    return data;
  }
}