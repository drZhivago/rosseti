class UserModel {
  int id;
  String fullName;
  String phone;
  int topicId;
  String email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  UserModel(
      {this.id,
      this.fullName,
      this.phone,
      this.topicId,
      this.email,
      this.commentsCount,
      this.ratingsCount,
      this.acceptedProposalsCount,
      this.deniedProposalsCount,
      this.proposalsCount});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    topicId = json['topic_id'];
    email = json['email'];
    commentsCount = json['comments_count'];
    ratingsCount = json['ratings_count'];
    acceptedProposalsCount = json['accepted_proposals_count'];
    deniedProposalsCount = json['denied_proposals_count'];
    proposalsCount = json['proposals_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['topic_id'] = this.topicId;
    data['email'] = this.email;
    data['comments_count'] = this.commentsCount;
    data['ratings_count'] = this.ratingsCount;
    data['accepted_proposals_count'] = this.acceptedProposalsCount;
    data['denied_proposals_count'] = this.deniedProposalsCount;
    data['proposals_count'] = this.proposalsCount;
    return data;
  }
}
