class VerifyModel {
  bool success;
  String error;
  String token;

  VerifyModel({this.success, this.error, this.token});

  VerifyModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    error = json['error'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['error'] = this.error;
    data['token'] = this.token;
    return data;
  }
}